const newObject = {
    title: 'newObject'
};

const newObject2 = {
    title: 'newObject2'
};

const objectToUpdate = {
    id: 101,
    title: 'Object to update'
};

const objectToUpdate2 = {
    id: 1,
    title: 'newObject2'
};

/**
 * Hozzunk létre egy új postot
 */
const addObject = async (object) => {
    const response = await fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        body: JSON.stringify(object),
        headers: {
            'Content-type': 'application/json',
        },
    });

    console.log('addObject() > response: ');
    console.log(await response.json());
};

const updateObject = async (object) => {
    const objectId = object.id;
    const response = await fetch('https://jsonplaceholder.typicode.com/posts/' + objectId, {
        method: 'PATCH',
        body: JSON.stringify(object),
        headers: {
            'Content-type': 'application/json',
        },
    });

    console.log('updateObject() > response: ');
    console.log(await response.json());
};

/**
 * Alakítsuk át a post létrehozást úgy,
 * hogy ha a kapott objektumnak van `id` mezője
 * akkor frissítsük a meglévő postot, egyébként hozzuk létre
 */
const saveObject = async (object) => {
    console.log('saveObject() called');
    if (object.id != null) {
        updateObject(object);
    } else {
        addObject(object);
    }
};

/**
 * Kérjük le a meglévő postokat
 * és listázzuk ki a konzolra
 * az első 10-nek a `title` mezőjét
 * (map és filter függvények használata javasolt)
 */
const getAll = async () => {
    const response = await fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'GET',
    });

    const objects = await response.json();
    console.log('getAll() > response for loop (LIMIT 10): ');
    for (let index = 0; index < 10; index++) {
        const element = objects[index];
        console.log(element.title);
    }
};

/**
 * Töröljünk egy postot
 */
const deleteObject = async (id) => {
    const response = await fetch('https://jsonplaceholder.typicode.com/posts/' + id, {
        method: 'DELETE',
    });

    console.log('deleteObject() > response.status: ');
    console.log(response.status);
};

const getById = async (id) => {
    const response = await fetch('https://jsonplaceholder.typicode.com/posts/' + id, {
        method: 'GET',
    });

    //console.log('getById() > response: ');
    //console.log(await response.json());
    return response.json();
};

/**
 * METHOD CALLS
 */
addObject(newObject);
updateObject(objectToUpdate);
saveObject(newObject2);
saveObject(objectToUpdate2);
deleteObject(101);
getAll();

/**
 * Kérjünk le egyszerre 5 különböző id-jú postot a Promise.all-al,
 * az eredményt írassuk a konzolra táblázatosan (console.table)
 */
const object1 = await getById(20);
const object2 = await getById(21);
const object3 = await getById(22);
const object4 = await getById(23);
const object5 = await getById(24);

const table = [object1, object2, object3, object4, object5];
console.table(table);


/** 
 * Csináljunk egy natív XHR kérést,
 * nézzük meg az event alapú működést,
 * és alakítsuk Promise-osra 
 */
const request = (id) => {
    return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open("GET", 'https://jsonplaceholder.typicode.com/posts/' + id);
        xhr.onload = () => {
            if (xhr.status >= 200) {
                resolve(xhr.response);
            } else {
                reject(xhr.status);
            }
        };
        xhr.send();
    });
};

request(26)
    .then(data => {
        console.log('request > XMLHttpRequest: ');
        console.log(data);
    })