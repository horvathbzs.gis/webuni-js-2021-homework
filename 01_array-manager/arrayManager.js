let numbers = [2, 14, 8, 83, 45];

const multiplyBy2 = numbers => {
    let newNumbers = new Array();
    numbers.forEach(number => {
        number = number * 2;
        newNumbers.push(number);
    });
    return newNumbers;
}

export const init = () => {
    const container = document.querySelector('#numbers');
    container.innerHTML = container.innerHTML + `
        <div class="number">
            A tömb: ${numbers} <br />
            Szorozva 2-vel: ${multiplyBy2(numbers)} <br />
            A tömb maximuma: ${Math.max(...numbers)} <br />
            A tömb elemeinek összege: ${numbers.reduce((acc, number) => acc + number, 0)}
        </div>
    `;
}
