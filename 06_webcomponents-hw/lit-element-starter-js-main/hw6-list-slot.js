/**
 * @license
 * Copyright 2019 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */

import { LitElement, html, css } from 'lit';


export class Hw6ListSlot extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        border: solid 1px gray;
        padding: 16px;
        max-width: 800px;
        width: 30%;
         font-size: 18px;
      }
      .container {
            border-radius: 10px;
            border-color: #E0E0E0;
            border-width: 1px;
            border-style: solid;
        }
        .title {
            text-align: center;
            font-weight: bottom;
            background-color: #E0E0E0;
            border-radius: 10px 10px 0 0;
        }

        li {
            list-style: none;
        }
    `;
  }

  constructor() {
    super();
  }

  static get properties() {
    return {

      title: { type: String },
      item1: { type: String },
      item2: { type: String },
      item3: { type: String }
    };
  }



  render() {
    return html`
      <div class="container">
            <div class="title bottom-border">${this.title}</div
            <ul>
                <li class="item bottom-border">
                    <slot name="item1"></slot>
                </li>
                <li class="item bottom-border">
                    <slot name="item2"></slot>
                </li>
                <li class="item">
                    <slot name="item3"></slot>
                </li>
            </ul>
       </div>
     `;
  }
}


window.customElements.define('hw6-list-slot', Hw6ListSlot);
