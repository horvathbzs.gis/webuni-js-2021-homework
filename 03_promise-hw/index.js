/**
 * Csináljunk egy Promise-t
 * amely 1 másodperc késleltetéssel
 * sikeresen végrehajtódik
 * (promise)
 */
const timeOutPromise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('SUCCESS');
  }, 1000);
});

timeOutPromise.then((value) =>
  console.log(`timeOutPromise() > then: ${value}`)
);

/**
 * Csináljunk egy Promise-t
 * amely 1 másodperc késleltetéssel
 * sikeresen végrehajtódik
 * Írjuk meg async / await-el is
 */
async function timeOutAsync() {
  const value = await timeOutPromise;
  console.log(`timeOutAsync() > ${value}`);
}

timeOutAsync();

/**
 * Alakítsuk át az előző kódot úgy,
 * hogy a promise 1 másodperc késleltetéssel
 * random vagy hibát dobjon vagy térjen vissza egy értékkel.
 */
const timeOutPromise2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    if (Math.floor(Math.random() * 2) == 1) {
      resolve('SUCCESS');
    } else {
      reject('ERROR');
    }
  }, 1000);
});

timeOutPromise2
  .then((value) => console.log(`timeOutPromise2() > then: ${value}`))
  .catch((error) => console.log(`timeOutPromise2() > error: ${error}`));

/**
 * Async/Await
 */
async function timeOutAsync2() {
  try {
    const value = await timeOutPromise2;
    console.log(`timeOutAsync2() > try: ${value}`);
  } catch (error) {
    console.log(`timeOutAsync2() > catch: ${error}`);
  }
}

timeOutAsync2();

/**
 * Indítsunk egy HTTP kérést tetszőleges címre,
 * a válaszát pedig írassuk ki a konzolra.
 */
const getName = async (name) => {
  try {
    const response = await fetch('https://api.genderize.io/?name=' + name, {
      method: 'GET',
    });
    console.log('getName() > response: ');
    console.log(await response.json());
  } catch (error) {
    console.log('getName() > error: ' + error);
  }
};

getName('Máté');

/**
 * Készítsünk hamburgert Promise-ok segítségével
 * A steak sütési ideje 15 másodperc
 * A zöldségek darabolása 5 másodperc
 * A zsemlék sütése 7 másodperc
 * A hamburger összerakása 10 másodperc,
 * de ez a művelet csak akkor kezdhető el, ha minden alapanyag rendelkezésre áll!
 */
const hamburgerBun = () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve('BUN');
    }, 7000);
  });
};

const steak = () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve('STEAK');
    }, 15000);
  });
};

const vegatables = () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve('VEGATABLE');
    }, 5000);
  });
};

const assembleBurger = (hamburgerBun, steak, vegatables) => {
  return new Promise((resolve => {
    setTimeout(() => {
      resolve([hamburgerBun, steak, vegatables, hamburgerBun])
    }, 10000);
  }))

};

Promise.all([hamburgerBun(), steak(), vegatables()]).then(async ([steak, hamburgerBun, vegatables]) => {
  console.log("Ingridients done")
  const burger = await assembleBurger(hamburgerBun, steak, vegatables);
  console.log(burger);
});