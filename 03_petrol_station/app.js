let carRollingId = 1;
const stationIdPrefix = 'station-'
const waitingQueue = [];
const statitions = [
    { id: 1, car: undefined, type: 'petrol', fillingTimeInSec: '3' },
    { id: 2, car: undefined, type: 'petrol', fillingTimeInSec: '3' },
    { id: 3, car: undefined, type: 'petrol', fillingTimeInSec: '3' },
    { id: 4, car: undefined, type: 'petrol', fillingTimeInSec: '3' },
    { id: 5, car: undefined, type: 'petrol', fillingTimeInSec: '3' },
    { id: 6, car: undefined, type: 'ac', fillingTimeInSec: '15' },
    { id: 7, car: undefined, type: 'dc', fillingTimeInSec: '7' }
];

addPetrolCar = () => {
    const car = { type: 'petrol' };
    const freeStation = hasFreeStation(car.type);

    addCar(car, freeStation);
}

addElectricCar = () => {
    const car = { type: 'electric' };
    const freeStation = hasFreeStation(car.type);

    addCar(car, freeStation);
}

addCar = (car, station) => {
    car.id = carRollingId++;

    if (station) {
        station.car = car;
        document.querySelector(`#${stationIdPrefix}${station.id}`).appendChild(getCarHTMLElement(car));

        fillCar(station)
            .then(station => {
                removeCarFromStation(station);
                callNextCar(station);
            })
    } else {
        addCarToWaitingQueue(car);
    }
}

addCarToWaitingQueue = car => {
    document.querySelector(`#queue`).appendChild(getCarHTMLElement(car));

    waitingQueue.push(car);
}

getCarHTMLElement = car => {
    const carElement = document.createElement('div');
    carElement.classList = 'car-item';
    carElement.id = `car-${car.id}`;

    carElement.innerHTML = `<span class="material-icons">${car.type === 'petro' ? 'directions_car' : 'electric_car'}</span>`

    return carElement;
}

fillCar = (station) => {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(station);
        }, station.fillingTimeInSec * 1000);
    });
}

callNextCar = station => {
    const car = getWaitingCar(station.type);

    if (car !== undefined) {
        addCar(car, station)
    }
}

getWaitingCar = stationType => {
    let car = undefined;

    if (stationType === 'ac' || stationType === 'dc') {
        car = waitingQueue.find(car => car.type === 'electric');
    } else {
        car = waitingQueue.find(car => car.type === 'petrol');
    }

    if (car !== undefined) {
        document.querySelector(`#car-${car.id}`).remove();
        waitingQueue.splice(waitingQueue.indexOf(car), 1);
    }

    return car;
}

removeCarFromStation = (station) => {
    station.car = undefined;
    document.querySelector(`#${stationIdPrefix}${station.id}`).innerHTML = '';
}

hasFreeStation = type => {
    return statitions
        .find(station => station.car === undefined && (station.type === type || (type === 'electric' && (station.type === 'ac' || station.type === 'dc'))));
}