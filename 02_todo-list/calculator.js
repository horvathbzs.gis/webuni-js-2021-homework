/**
 * Gets the number of todos with todo.isCompleted = true,
 * and devides this number with the total number of todos.
 * @param {*} todos 
 * @returns number, rate of isCompleted = true todos
 */
export const calculateProgess = todos => todos.filter(t => t.isCompleted).length / todos.length;

/**
 * Finds the maximum value of todo.difficulty among a list of todos.
 * @param {*} todos 
 * @returns number, max value of todo.difficulty
 */
export const findMostDifficult = todos => Math.max(...todos.map(todo=> todo.difficulty));

/*
export const calculateTodoRate = todos => {
    let isDoneTrue = 0;

    todos.forEach(todo => {
        //isDoneTrue++;
        todo.isDone ? isDoneTrue++ : '';
    });

    return Math.round((isDoneTrue / todos.length) * 100);
}
*/