import { initTodos } from "./todos.js";

/**
 * Actions after refreshing page.
 */
document.addEventListener('DOMContentLoaded', () => {
    initTodos();
});