import { calculateProgess, findMostDifficult } from "./calculator.js";

/**
 * setup data
 */
const todo1 = {
    name: 'Clean bathroom',
    isCompleted: false,
    difficulty: 4
};

const todo2 = {
    name: 'Laundry',
    isCompleted: false,
    difficulty: 3
};

const todo3 = {
    name: 'Dishes',
    isCompleted: false,
    difficulty: 2
};

const todos = [
    todo1,
    todo2,
    todo3
];

const difficultyOptions = new Array (1, 2, 3, 4, 5);

/**
 * actions after opening 
 */
export const initTodos = () => { 
    createDifficultyOption();
    saveTodo();
    sortTodo();
    refreshTodos();
}

/**
 * Adds options to html select todo difficulty list 
 * based on difficultyOptions array.
 */
const createDifficultyOption = () => {
    const difficultyList = document.querySelector('#new-todo-difficulty-list');
    difficultyOptions.forEach(diffOption => {
        let option = document.createElement("option");
        option.text = diffOption;
        option.className = "new-todo-difficulty-option";
        difficultyList.add(option);
    });
};

/**
 * Creates html tags to display todo data.
 */
const displayTodos = () => {
    const container = document.querySelector('#todos');
    container.innerHTML = '';
    todos.forEach((todo, index) => {
        container.innerHTML = container.innerHTML + `
            <div class="${todo.isCompleted ? 'complete' : 'incomplete'}">
                <input ${todo.isCompleted ? 'checked' : ''} type="checkbox">
                ${index + 1}. ${todo.name} - ${todo.difficulty}
                <input id="bt-delete-todo${index}" class="btn-delete" type="button" value="X">
            </div>
        `;
    });
};

/**
 * Todos raw checkbox and delete button event listener.
 */
const addEventListeners = () => {

    /**
     * Updates todo.isCompleted if checkbox changes on UI.
     */
    document.querySelectorAll('input[type=checkbox]').forEach((checbox, index) => {
        checbox.addEventListener('change', () => {
            todos[index].isCompleted = checbox.checked;
            refreshTodos();
        });
    });

    /**
     * Queries btn-delete class buttons,
     * deletes the todo where delete button has been clicked.
     */
    document.querySelectorAll('.btn-delete').forEach((button, index) => {
        button.addEventListener('click', () => {
            todos.splice(index, 1);
            refreshTodos();
        });
    });
};

/**
 * Sets progress span with the percent rate of completed todos.
 * Calls calculator.calculateProgress() method.
 */
const updateProgress = () => {
    document.querySelector('#progess').textContent = `${Math.round(calculateProgess(todos) * 100)}%`;
};

/**
 * Finds the todo that has the highest difficulty number. Calls calculator.findMostDifficult().
 * Sets most difficult span with todo.name and todo.difficulty.
 */
const updateMostDifficult = () => {
    let mostDifficult = todos.find(t => t.difficulty === findMostDifficult(todos));
    document.querySelector('#most-difficult').textContent = `${mostDifficult.name} - ${mostDifficult.difficulty}`;
};

/**
 * After bt-save button clicked, the new todo is added to todo list. 
*/
const saveTodo = () => {
    document.querySelector('#bt-save').addEventListener('click', () => {
        
        // --- get input values ---
        const name = document.querySelector('#new-todo-name').value;
        const difficulty = Number(document.querySelector('#new-todo-difficulty-list').value);
        console.log('todos > saveTodo > name: ' + name);
        console.log('todos > saveTodo > difficulty: ' + difficulty);

        // --- add new todo to todos ---
        if (name != '') {
            const newTodo = createTodo(name, difficulty);
            todos.push(newTodo);
            console.log('todos > saveTodo > todos: ');
            todos.forEach(todo => console.log(todo));
        }

        // --- clear input ---
        document.querySelector('#new-todo-name').value = '';
        document.querySelector('#new-todo-difficulty-list').value = difficultyOptions[0];
        refreshTodos();
    });
};

/**
 * Creates a new todo with name and difficulty params, and isCompleted = false.
 * @param {*} name tood.name
 * @param {*} difficulty todo.difficulty
 * @return todo
 */
const createTodo = (name, difficulty) => {
    let newTodo = {
        name: name,
        isCompleted: false,
        difficulty: difficulty
    }
    return newTodo;
};

const sortTodo = () => {
    document.querySelector('#bt-sort-asc').addEventListener('click', () => {
        todos.sort((t1, t2) => t1.difficulty < t2.difficulty ? -1 : (t1.difficulty === t2.difficulty ? 0 : 1));
        refreshTodos();
    });

    document.querySelector('#bt-sort-desc').addEventListener('click', () => {
        todos.sort((t1, t2) => t1.difficulty < t2.difficulty ? 1 : (t1.difficulty === t2.difficulty ? 0 : -1));
        refreshTodos();
    });
};

const refreshTodos = () => {
    displayTodos();
    addEventListeners();
    updateProgress();
    updateMostDifficult();
};